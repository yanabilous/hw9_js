let input_array = ["Kharkiv", "Kiev", ["Borispol", ["Borispol", "Irpin"]], "Odessa", "Lviv", "Dnieper"];

function getElement(array, parent = document.body) {
    let ulEl = document.createElement("ul");
    array.map(item => {
        let itemLi = document.createElement("li");
        if (Array.isArray(item)) {
            getElement(item, itemLi);
        } else {
            itemLi.innerHTML = item;
        }
        ulEl.appendChild(itemLi);
    });
    parent.appendChild(ulEl);
    setTimeout(() => parent.remove(), 3000);
}


getElement(input_array);


let timeleft = 3;
let timerBlock = document.createElement("div");
timerBlock.innerText = "Timer: ";
let timerNumber = document.createElement("span");
timerNumber.innerText = `${timeleft}`;
timerBlock.appendChild(timerNumber);
document.body.prepend(timerBlock);


let downloadTimer = setInterval( () =>{
    timeleft--;
    timeleft <= 0 ? clearInterval(downloadTimer) : timerNumber.innerHTML = `${timeleft}`;
}, 1000);

